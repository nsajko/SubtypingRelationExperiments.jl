# SubtypingRelationExperiments

An experiment for finding counterexamples to the preorder properties
that Julia's subtyping is supposed to satisfy.

The results are produced by running the following script:

```julia
using SubtypingRelationExperiments
SubtypingRelationExperiments.Main.experiment()
```
