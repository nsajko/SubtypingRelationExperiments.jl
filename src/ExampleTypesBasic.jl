# Copyright © 2023 Neven Sajko

module ExampleTypesBasic

abstract type A end  # abstract
struct   C <: A end  # concrete
Base.show(io::IO, ::C      ) = print(io, "C()")
Base.show(io::IO, ::Type{A}) = print(io, "A")
Base.show(io::IO, ::Type{C}) = print(io, "C")
const example = (Union{}, C, A, Any)

end
