# Copyright © 2023 Neven Sajko

module SubtypingRelationExperiments

include("ExampleTypesBasic.jl")
include("ExampleTypesTuple.jl")
include("ExampleTypesParametricTuple.jl")
include("ExampleTypesParametricStruct.jl")
include("ExampleTypesParametric.jl")
include("ExampleTypes.jl")

include("RelationChecking.jl")

include("Main.jl")

end
