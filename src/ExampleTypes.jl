# Copyright © 2023 Neven Sajko

module ExampleTypes

import ..ExampleTypesBasic, ..ExampleTypesParametric, ..ExampleTypesTuple

const ETB = ExampleTypesBasic
const ETP = ExampleTypesParametric
const ETT = ExampleTypesTuple

const example = let
  type_application = function(V::Type, T::Type)
    if isconcretetype(V)
      V
    else
      try
        V{T}
      catch
        nothing
      end
    end
  end

  basic = ETB.example
  tuple = ETT.example
  incomplete = ETP.example

  applied = []

  for V ∈ incomplete, B ∈ basic
    a = type_application(V, B)
    (a === nothing) || push!(applied, a)
  end

  (basic..., tuple..., incomplete..., map(identity, applied)...)
end

end
