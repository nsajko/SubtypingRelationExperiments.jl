# Copyright © 2023 Neven Sajko

module ExampleTypesParametricTuple

import ..ExampleTypesBasic

const Bas = ExampleTypesBasic

const example = let
  ret = []

  p = let ret = ret
    e -> push!(ret, e)
  end

  Abs = Bas.A
  bas_lenm1 = length(Bas.example) - 1
  fi_bas = firstindex(Bas.example)

  # over all valid, `∈Bas.example`, lower and upper bounds on `X`
  for i ∈ 0:bas_lenm1, j ∈ i:bas_lenm1
    L = Bas.example[fi_bas + i]
    U = Bas.example[fi_bas + j]

    p(Tuple{X                    } where {L<:X<:U})
    p(Tuple{X,X                  } where {L<:X<:U})
    p(Tuple{Any,X                } where {L<:X<:U})
    p(Tuple{Abs,X                } where {L<:X<:U})
    p(Tuple{X,Any                } where {L<:X<:U})
    p(Tuple{X,Abs                } where {L<:X<:U})
    p(Tuple{X,        Vararg{Any}} where {L<:X<:U})
    p(Tuple{X,        Vararg{Abs}} where {L<:X<:U})
    p(Tuple{X,X,      Vararg{Any}} where {L<:X<:U})
    p(Tuple{X,X,      Vararg{Abs}} where {L<:X<:U})
    p(Tuple{Any,X,    Vararg{Any}} where {L<:X<:U})
    p(Tuple{Any,X,    Vararg{Abs}} where {L<:X<:U})
    p(Tuple{Abs,X,    Vararg{Any}} where {L<:X<:U})
    p(Tuple{Abs,X,    Vararg{Abs}} where {L<:X<:U})
    p(Tuple{X,Any,    Vararg{Any}} where {L<:X<:U})
    p(Tuple{X,Any,    Vararg{Abs}} where {L<:X<:U})
    p(Tuple{X,Abs,    Vararg{Any}} where {L<:X<:U})
    p(Tuple{X,Abs,    Vararg{Abs}} where {L<:X<:U})
    p(Tuple{          Vararg{X  }} where {L<:X<:U})
    p(Tuple{X,        Vararg{X  }} where {L<:X<:U})
    p(Tuple{Any,      Vararg{X  }} where {L<:X<:U})
    p(Tuple{Abs,      Vararg{X  }} where {L<:X<:U})
    p(Tuple{X,X,      Vararg{X  }} where {L<:X<:U})
    p(Tuple{Any,X,    Vararg{X  }} where {L<:X<:U})
    p(Tuple{Abs,X,    Vararg{X  }} where {L<:X<:U})
    p(Tuple{X,Any,    Vararg{X  }} where {L<:X<:U})
    p(Tuple{X,Abs,    Vararg{X  }} where {L<:X<:U})
    p(Tuple{Any,Any,  Vararg{X  }} where {L<:X<:U})
    p(Tuple{Any,Abs,  Vararg{X  }} where {L<:X<:U})
    p(Tuple{Abs,Any,  Vararg{X  }} where {L<:X<:U})
    p(Tuple{Abs,Abs,  Vararg{X  }} where {L<:X<:U})
  end

  (map(identity, ret)...,)
end

end
