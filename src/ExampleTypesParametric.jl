# Copyright © 2023 Neven Sajko

module ExampleTypesParametric

import ..ExampleTypesParametricStruct, ..ExampleTypesParametricTuple

const Str = ExampleTypesParametricStruct
const Tup = ExampleTypesParametricTuple
const example = (Str.example..., Tup.example...)

end
