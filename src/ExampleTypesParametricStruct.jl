# Copyright © 2023 Neven Sajko

module ExampleTypesParametricStruct

import ..ExampleTypesBasic

const Bas = ExampleTypesBasic

# with all valid, `∈Bas.example`, lower and upper bounds on `X`
struct AA{         X <: Union{}} end
struct AB{         X <: Bas.C  } end
struct AC{         X <: Bas.A  } end
struct AD{         X <: Any    } end
struct BB{Bas.C <: X <: Bas.C  } end
struct BC{Bas.C <: X <: Bas.A  } end
struct BD{Bas.C <: X <: Any    } end
struct CC{Bas.A <: X <: Bas.A  } end
struct CD{Bas.A <: X <: Any    } end
struct DD{Any   <: X <: Any    } end

const example = (AA,AB,AC,AD, BB,BC,BD, CC,CD, DD)

end
