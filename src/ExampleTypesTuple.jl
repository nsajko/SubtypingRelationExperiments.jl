# Copyright © 2023 Neven Sajko

module ExampleTypesTuple

import ..ExampleTypesBasic

const Bas = ExampleTypesBasic

const example = let
  # no `Union{}` because it's not allowed in some Julia versions
  bas = (Bas.C, Bas.A, Any)
  (
    Tuple{},
    (Tuple{X} for X ∈ bas)...,
    (Tuple{X,Y} for X ∈ bas, Y ∈ bas)...,
    (Tuple{Vararg{X}} for X ∈ bas)...,
    (Tuple{X,Vararg{Y}} for X ∈ bas, Y ∈ bas)...,
    (Tuple{X,Y,Vararg{Z}} for X ∈ bas, Y ∈ bas, Z ∈ bas)...,
    (Tuple{X,Y,Z,Vararg{W}} for X ∈ bas, Y ∈ bas, Z ∈ bas, W ∈ bas)...,
  )
end

end
