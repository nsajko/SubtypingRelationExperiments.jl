# Copyright © 2023 Neven Sajko

module RelationChecking

function check_reflexivity(pred, iter)
  ret = Vector{Any}()

  for e ∈ iter
    pred(e, e) || push!(ret, e)
  end

  map(identity, ret)::AbstractVector
end

function check_transitivity(pred, iter)
  ret = Vector{NTuple{3,Any}}()

  for a ∈ iter, b ∈ iter, c ∈ iter
    if pred(a, b) && pred(b, c)
      pred(a, c) || push!(ret, (a, b, c))
    end
  end

  map(identity, ret)::AbstractVector{<:NTuple{3,Any}}
end

function id_unique(iter)
  f = x -> (x, nothing)
  kvs = Iterators.map(f, iter)
  dict = IdDict{Any,Nothing}(kvs)
  ks = collect(keys(dict))
  map(identity, ks)
end

function report_preorder(io, pred, iterator)
  p = let io = io
    function(m)
      print(io, m)
      flush(io)
    end
  end

  p("checking if the predicate $pred is a preorder relation\n")
  p("  * element count before deduplication:  $(length(iterator))\n")

  iter = id_unique(iterator)
  p("  * element count after  deduplication:  $(length(iter))\n")

  refl_failures = check_reflexivity(pred, iter)
  p("  * reflexivity  counterexample count before deduplication:  $(length(refl_failures))\n")

  refl_fails = id_unique(refl_failures)
  p("  * reflexivity  counterexample count after  deduplication:  $(length(refl_fails))\n")

  tran_failures = check_transitivity(pred, iter)
  p("  * transitivity counterexample count before deduplication:  $(length(tran_failures))\n")

  tran_fails = id_unique(tran_failures)
  p("  * transitivity counterexample count after  deduplication:  $(length(tran_fails))\n")

  refl_fails_str = sort!(map(string, refl_fails))
  tran_fails_str = sort!(map(string, tran_fails))

  p("\n")
  p("reflexivity counterexamples:\n")
  p("[\n")

  for f ∈ refl_fails_str
    p("  $f,\n")
  end

  p("]\n")

  p("\n")
  p("transitivity counterexamples:\n")
  p("[\n")

  for f ∈ tran_fails_str
    p("  $f,\n")
  end

  p("]\n")
end

end
