# Copyright © 2023 Neven Sajko

module Main

import InteractiveUtils, ..ExampleTypes, ..RelationChecking

default_subtyping_relation(L, R) = (L <: R)::Bool

function experiment(
  io = stdout, pred = default_subtyping_relation, types = ExampleTypes.example,
)
  InteractiveUtils.versioninfo(io)
  print(io, "\n\n\n")
  flush(io)
  RelationChecking.report_preorder(
    io, pred, ExampleTypes.example,
  )
end

end
